package lrucache

import "container/list"

type Cache interface {
	// Get returns value associated with the key.
	//
	// The second value is a bool that is true if the key exists in the cache,
	// and false if not.
	Get(key int) (int, bool)
	// Set updates value associated with the key.
	//
	// If there is no key in the cache new (key, value) pair is created.
	Set(key, value int)
	// Range calls function f on all elements of the cache
	// in increasing access time order.
	//
	// Stops earlier if f returns false.
	Range(f func(key, value int) bool)
	// Clear removes all keys and values from the cache.
	Clear()
}

type Pair struct {
	key   int
	value int
}

type newCache struct {
	size     int
	elements *list.List
	maps     map[int]*list.Element
}

func NewCache(cap int) newCache {
	return newCache{
		size:     cap,
		elements: list.New().Init(),
		maps:     make(map[int]*list.Element),
	}
}

func (c newCache) Get(key int) (int, bool) {
	if c.size == 0 {
		return 0, false
	}

	elem, ok := c.maps[key]

	if !ok {
		return 0, false
	}

	pair := elem.Value.(Pair)
	c.elements.MoveToFront(elem)

	return pair.value, ok
}

func (c newCache) Set(key int, value int) {
	if c.size == 0 {
		return
	}

	pair := Pair{
		key:   key,
		value: value,
	}

	elem, ok := c.maps[key]

	if !ok {
		elem = c.elements.PushFront(pair)
	}

	elem.Value = pair
	c.elements.MoveToFront(elem)
	c.maps[key] = elem

	if c.elements.Len() <= c.size {
		return
	}

	elem = c.elements.Back()
	pair = elem.Value.(Pair)
	delete(c.maps, pair.key)
	c.elements.Remove(elem)
}

func (c newCache) Range(f func(key, value int) bool) {
	if c.size == 0 {
		return
	}

	for elem := c.elements.Back(); elem != nil; elem = elem.Prev() {
		pair := elem.Value.(Pair)
		ok := f(pair.key, pair.value)

		if !ok {
			return
		}
	}
}

func (c newCache) Clear() {
	for k := range c.maps {
		delete(c.maps, k)
	}

	elem := c.elements.Front()

	for elem != nil {
		c.elements.Remove(elem)
		elem = c.elements.Front()
	}
}
