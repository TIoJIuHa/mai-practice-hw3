package ciletters

import (
	_ "embed"
	"strings"
	"text/template"
)

//go:embed letter.txt
var s string

func GetLast10Lines(logs string) []string {
	arr := strings.Split(logs, "\n")
	if len(arr) >= 10 {
		return arr[len(arr)-10:]
	}
	return arr
}

func MakeLetter(n *Notification) (string, error) {
	var out strings.Builder

	tmpl, err := template.New("test").Funcs(template.FuncMap{
		"hash": func() string { return n.Commit.Hash[:8] },
		"logs": GetLast10Lines,
	}).Parse(s)

	if err != nil {
		return "", err
	}

	err = tmpl.Execute(&out, n)
	if err != nil {
		return "", err
	}

	return strings.ReplaceAll(out.String(), "\r\n", "\n"), err
}
