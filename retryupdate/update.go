package retryupdate

import (
	"errors"

	"github.com/gofrs/uuid"
	"gitlab.com/TIoJIuHa/mai-practice-hw3/retryupdate/kvapi"
)

func UpdateValue(c kvapi.Client, key string, updateFn func(oldValue *string) (newValue string, err error)) error {
	var auth *kvapi.AuthError
	var conflict *kvapi.ConflictError
	var oldValue *string = nil
	var oldVersion uuid.UUID

Loop:
	for {
		res, err := c.Get(&kvapi.GetRequest{Key: key})

		switch {
		case errors.Is(err, kvapi.ErrKeyNotFound):
			break Loop
		case err == nil:
			oldValue = &res.Value
			oldVersion = res.Version
			break Loop
		case errors.As(err, &auth):
			return err
		}
	}

	updated, err := updateFn(oldValue)

	if err != nil {
		return err
	}

	newVersion := uuid.Must(uuid.NewV4())

	for {
		_, err := c.Set(&kvapi.SetRequest{Key: key, Value: updated, OldVersion: oldVersion, NewVersion: newVersion})
		switch {
		case errors.Is(err, kvapi.ErrKeyNotFound):
			oldVersion = uuid.UUID{}
			updated, err = updateFn(nil)
		case err == nil || errors.As(err, &auth):
			return err
		case errors.As(err, &conflict):
			if conflict.ExpectedVersion == newVersion {
				return nil
			}
			return UpdateValue(c, key, updateFn)
		}
	}
}
