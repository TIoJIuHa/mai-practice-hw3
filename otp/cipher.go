package otp

import (
	"io"
)

type streamCipherReader struct {
	r    io.Reader
	prng io.Reader
}

func (scr *streamCipherReader) Read(b []byte) (int, error) {
	n, err := scr.r.Read(b)
	prngBuffer := make([]byte, n)
	scr.prng.Read(prngBuffer)

	for i := 0; i < n; i++ {
		b[i] = b[i] ^ prngBuffer[i]
	}

	return n, err
}

func NewReader(r io.Reader, prng io.Reader) io.Reader {
	return &streamCipherReader{r: r, prng: prng}
}

type streamCipherWriter struct {
	w    io.Writer
	prng io.Reader
}

func (scw *streamCipherWriter) Write(b []byte) (int, error) {
	prngBuffer := make([]byte, len(b))
	scw.prng.Read(prngBuffer)

	for i := 0; i < len(b); i++ {
		prngBuffer[i] = prngBuffer[i] ^ b[i]
	}

	return scw.w.Write(prngBuffer)
}

func NewWriter(w io.Writer, prng io.Reader) io.Writer {
	return &streamCipherWriter{w: w, prng: prng}
}
